<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Basic Struts 2 Application - Welcome</title>
    </head>
    <body>
        <h1>Welcome To Struts 2!</h1>

        <!--struts2的核心控制器默认会处理以.action为后缀的url，或者是没有任何后缀的url-->
        <a href="${pageContext.request.contextPath}/n1/hello">第一个链接</a>
        <a href="${pageContext.request.contextPath}/n1/hello.do">第二个链接</a>



        <a href="${pageContext.request.contextPath}/n1/hello2.do">hello2</a>

        <a href="${pageContext.request.contextPath}/n1/hello3.do">hello3</a>


        <a href="${pageContext.request.contextPath}/n1/hello4.do">hello4</a>
    </body>
</html>