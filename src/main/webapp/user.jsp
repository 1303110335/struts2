<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
    <head>
        <meta charset="UTF-8">
        <title>用户管理</title>
    </head>
    <body>
        <!--struts2的核心控制器默认会处理以.action为后缀的url，或者是没有任何后缀的url-->
        <%--<a href="${pageContext.request.contextPath}/addUser">添加用户</a>

        <a href="${pageContext.request.contextPath}/updateUser">更新用户</a>

        <a href="${pageContext.request.contextPath}/deleteUser">删除用户</a>

        <a href="${pageContext.request.contextPath}/findUser">查找用户</a>--%>


        <%--<a href="${pageContext.request.contextPath}/add_User">添加用户</a>

        <a href="${pageContext.request.contextPath}/update_User">更新用户</a>

        <a href="${pageContext.request.contextPath}/delete_User">删除用户</a>

        <a href="${pageContext.request.contextPath}/find_User">查找用户</a>--%>

        <a href="${pageContext.request.contextPath}/user!addUser">添加用户</a>

        <a href="${pageContext.request.contextPath}/user!updateUser">更新用户</a>

        <a href="${pageContext.request.contextPath}/user!deleteUser">删除用户</a>

        <a href="${pageContext.request.contextPath}/user!findUser">查找用户</a>
    </body>


</html>