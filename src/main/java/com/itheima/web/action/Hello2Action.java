package com.itheima.web.action;

import com.opensymphony.xwork2.Action;

/**
 * 通过实现接口的方式创建动作类
 */
public class Hello2Action implements Action {
    @Override
    public String execute() throws Exception {
        System.out.println("Hello2Action的execute方法执行了....");
        return SUCCESS;
    }
}
