package com.itheima.web.action;

/**
 * first action class
 *  动作类：
 *      它是一个概念。ta就是struts2框架中用于处理请求的类
 *      处理请求都写动作类
 */
public class HelloAction {

    /**
     * 动作方法都有编写规范
     * 1.访问修饰符都是public
     * 2.方法的返回值一般都是String(都可以是void)
     * 3.方法都没有参数
     * @return
     */
    public String sayHello() {
        System.out.println("HelloAction 的 sayHello方法执行了" + this);
        return "success";
    }


}
